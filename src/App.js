import React from 'react';
import './App.css';
import {createUser} from './UserForm/operations'

function mockRequest() {
  var mockData = {
    username:'Gu6',
    email:'rayluis0403@gmail.com',
    name:'Gol2',
    lastname:'Eso',
    identification:'x',
    phone:'8494568525',
    phone2:'8459635241',
    identificationType:1,
    locale:'en-us',
    active:true
  }
  
  createUser(mockData);
}

function App() {
  return (
    <div className="App">
      

      <button onClick={mockRequest}>Click para enviar request</button>
    </div>
  );
}

export default App;
