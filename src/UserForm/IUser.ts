


export interface IUser {
    username:string
	email:string
	name:string
	lastname:string
	identification:string
	phone: string
	phone2:string
	identificationType:number
	locale:string
    active:boolean
    roles:string[]
}

