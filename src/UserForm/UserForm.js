import * as React from 'react'
import "antd/dist/antd.css";
import "./UserForm.css";
import { Row, Col, Icon, Select, Button, Alert } from "antd";
import { createUser } from './operations'

const { Option } = Select;

export default class UserForm  extends React.Component {

  state = {
    userName: '',
    name: '',
    lastName: '',
    identificationType: 'IdentityDocumentEsDo',
    identification: '',
    email: '',
    phone1: '',
    phone2: '',
    location: 'es-es',
    userSaved: false,
    errorMessage: undefined,
    loading: false
  }

  resetState() {
    this.setState({
      userName: '',
      name: '',
      lastName: '',
      identificationType: 'IdentityDocumentEsDo',
      identification: '',
      email: '',
      phone1: '',
      phone2: '',
      location: 'es-es',
      userSaved: false,
      errorMessage: undefined,
      loading: false
    })
  }

  onChangeIdentificationType (value, option) {
    this.setState({identificationType: value});
  }

  onChangeLocation (value,option) {
    this.setState({location: value});
  }

  onSubmitForm(){
    
    this.setState({loading: true})

    const {userName, name, lastName, identificationType, identification, email, phone1, phone2, location} = this.state

    var formData = {
      username:userName,
      active:true,
      name:name,
      lastname:lastName,
      identificationType:identificationType,
      identification:identification,
      email:email,
      phone:phone1,
      phone2:phone2,
      locale:location,
      restrictedToOwnedDocuments: true,
      roles:[ { name: process.env.REACT_APP_ROLE_NAME}]
    }
   
    createUser(formData).then(response => {
        
      console.log('response', response);

      if(response.success){
          this.setState({userSaved: true, errorMessage: undefined, loading: false});
      }
      else {
        
        var message = response.message? response.message : response.Message;

        if(response.ModelState){
           message = ''
          response.ModelState.rawValue.forEach(text => message += `${text} `);
        }
        this.setState({userSaved: false, errorMessage: message, loading: false});
      }
      
    });
  }

  createNewUser = () => {
    this.resetState();
  }

  onChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  AlertContent = () => {
    const { name, lastName } = this.state

    return (<div className="alert-content">
            <Alert
              message={`Usuario ${name} ${lastName} creado Correctamente`}
              type="success"
              showIcon
            />
            <Row type="flex" justify="center">
              <Col span={16}>
                <Button type="primary" className="btn-alert" block onClick={this.createNewUser}>
                  <Icon type="check" /> Crear nuevo usuario
                </Button>
              </Col>
            </Row>
          </div>
    )
  }

  warningBox = () => {
    return (<div className="warnning-box">
    <Alert
      message="Error"
      description={this.state.errorMessage}
      type="error"
      showIcon
    />
  </div>)
  }

  render(){
    const {userName, name, lastName, identificationType, identification, email, phone1, phone2, location
    , userSaved, errorMessage, loading} = this.state

    return (
      <div className="user-Form">
        <div style={ userSaved ? {}: {display:'none'}} id="alert-box" className="alert-box">
          {this.AlertContent()}
        </div>
        <div id="form-box" className="form-box">
          <div>
            <div className="logo"></div>
            <h2>Registro de nuevos usuarios</h2>
            {!errorMessage ? <div /> : this.warningBox()}
            <form>
              <Row>
                <Col span={12} className="columna1">
                  <div className="form-group">
                    <label className="form-label">Nombre de Usuario</label>
                    <input className="form-input" required name={"userName"} value={userName} onChange={this.onChange} />
                  </div>
                  <div className="form-group">
                    <label className="form-label">Nombre</label>
                    <input className="form-input" required  name={"name"} value={name} onChange={this.onChange}  />
                  </div>
                  <div className="form-group">
                    <label className="form-label">Apellidos</label>
                    <input className="form-input"  required  name={"lastName"} value={lastName} onChange={this.onChange}  />
                  </div>
                  <div className="form-group">
                    <label className="form-label">Tipo de Identificacion</label>
                    <Select
                      name={"identificationType"}
                      value={identificationType}
                      defaultValue="cedula"
                      onChange={(v, o) => {this.onChangeIdentificationType(v, o)}}
                    >
                      <Option value="" disabled="disabled">- Seleccione tipo de identificación - <Icon type="idcard" /></Option>
                      <Option value="IdentityDocumentEsDo" >Cédula de Identidad y Electoral</Option>
                      <Option value="Passport">Pasaporte</Option>
                      <Option value="TaxpayerIdentificationNumberEsDo">Registro Nacional de Contribuyente</Option>
                      <Option value="Other">Otro</Option>
                    </Select>
                  </div>

                  <div className="form-group">
                    <label className="form-label">Identificacion</label>
                    <input className="form-input"  required  name={"identification"} value={identification} onChange={this.onChange}  />
                  </div>
                </Col>
                <Col span={12} className="columna2">
                  <div className="form-group">
                    <label className="form-label">Email</label>
                    <input className="form-input"  required  name={"email"} value={email} onChange={this.onChange} />
                  </div>
                  <div className="form-group">
                    <label className="form-label">Phone1</label>
                    <input className="form-input"  required  name={"phone1"} value={phone1} onChange={this.onChange} />
                  </div>
                  <div className="form-group">
                    <label className="form-label">Phone2</label>
                    <input className="form-input"  required   name={"phone2"} value={phone2} onChange={this.onChange} />
                  </div>

                  <div className="form-group">
                    <label className="form-label">Idioma de interfaz</label>
                    <Select
                      name={"location"}
                      value={location}
                      style={{ width: 200 }}
                      onChange={(v, o) => {this.onChangeLocation(v, o)}}
                    >
                      <Option value="" disabled="disabled">- Seleccione idioma -</Option>
                      <Option value="es-es">Español</Option>
                      <Option value="en-us">English - United States</Option>
                      <Option value="fr-fr">Français - France</Option>
                    </Select>
                  </div>
                </Col>
              </Row>
              <Row type="flex" justify="end">
                <Col span={6}>
                  <Button type="primary" block onClick={() => this.onSubmitForm()} loading={loading}>
                    <Icon type="check" /> Crear usuario
                  </Button>
                </Col>
              </Row>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
