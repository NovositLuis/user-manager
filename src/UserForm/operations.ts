
import { IUser } from './IUser'

var config = {
    url: '/api/v1/users',
    user: process.env.REACT_APP_ADMIN_USER,
    password: process.env.REACT_APP_ADMIN_PASS,
    apiKey: process.env.REACT_APP_API_KEY || '',
    resourceUrl:"/Admin/AdminServiceJson.svc"
};

export function createUser(user: IUser){
    var options = {
        method: 'post',
        credential: 'same-origin',
        headers: {
            'X-API-KEY': config.apiKey,
          'Content-Type': 'application/json',
          'Authorization': 'Basic '+btoa(config.user + ':'+ config.password),
        },
        body: JSON.stringify(user)
      };

      function callback(httpResponse:any){

          if(httpResponse.ok){
            return Promise.resolve({success: true});
          }

          return httpResponse.json();
       };
    
    return fetch(config.url, options).then(callback);
}

export function getLocales(){
    const uri = config.resourceUrl + "/locales";
    var options = {
        method: 'get',
        credential: 'same-origin',
        headers: {
            'X-API-KEY': config.apiKey,
            'Content-Type': 'application/json',
            'Authorization': 'Basic '+btoa(config.user + ':'+ config.password),
        }
      };

      function callback(httpResponse:any){ 
          console.log('httpResponse', httpResponse);
       };
    
    fetch(uri, options).then(callback);
}

export function getPeriodTypes(){
    const uri = config.resourceUrl + "/validityPeriodTypes";
    var options = {
        method: 'get',
        //credential: 'same-origin',
        headers: {
            //'X-API-KEY': config.apiKey,
          'Content-Type': 'application/json',
          //'Authorization': 'Basic '+btoa(config.user + ':'+ config.password),
        }
      };

      function callback(httpResponse:any){ 
          console.log('httpResponse', httpResponse);
       };
    
    fetch(uri, options).then(callback);

}

export function getIdentificationTypes(){
    const uri = config.resourceUrl + "/identificationTypes";
    var options = {
        method: 'get',
        credential: 'same-origin',
        headers: {
            'X-API-KEY': config.apiKey,
            'Content-Type': 'application/json',
            'Authorization': 'Basic '+btoa(config.user + ':'+ config.password),
        }
      };

      function callback(httpResponse:any){ 
          console.log('httpResponse', httpResponse);
       };
    
    fetch(uri, options).then(callback);

}

export function getRoles(){return []}

